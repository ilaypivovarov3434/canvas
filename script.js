onload = () => {

  const canvas = document.getElementById(`canvas`);
  context = canvas.getContext(`2d`);
  ctx = canvas.getContext(`2d`);
  ctx.translate(200, 200);
  ctx.rotate((3 * Math.PI) / 2);

  function clock_draw() {
      var now = new Date();
      var mili = now.getMilliseconds();
      var sec = now.getSeconds();
      var minute = now.getMinutes();
      var hour = now.getHours();

      ctx.clearRect(-200, -200, canvas.width, canvas.height);

      ctx.strokeStyle = "black";
      ctx.fillStyle = "black";
      ctx.lineWidth = 2;

      ctx.beginPath();
      ctx.arc(0, 0, 140, 0, 2 * Math.PI, false);
      ctx.stroke();

      ctx.save();
      ctx.beginPath();
      for (var i = 0; i < 60; i++) {
          ctx.rotate(Math.PI / 30);
          ctx.moveTo(130, 0);
          ctx.lineTo(140, 0);
      }
      ctx.stroke();
      ctx.restore();

      ctx.save();
      ctx.rotate(-Math.PI / 3);
      ctx.beginPath();
      for (var i = 0; i < 12; i++) {
          var j = i * Math.PI / 30;
          ctx.rotate(-Math.PI / 6);
          ctx.moveTo(120, 0);
          ctx.lineTo(140, 0);
          ctx.stroke();
      }
      ctx.stroke();
      ctx.restore();

      ctx.save();
      ctx.rotate((Math.PI / 30) * (sec) + ((Math.PI / (30240)) * mili));
      ctx.lineWidth = 2;
      ctx.strokeStyle = "red";
      ctx.beginPath();
      ctx.moveTo(0, 0);
      ctx.lineTo(120, 0);
      ctx.stroke();
      ctx.restore();

      ctx.save();
      ctx.rotate((Math.PI / 30 * minute) + ((Math.PI / 1800) * sec));
      ctx.lineWidth = 3;
      ctx.beginPath();
      ctx.moveTo(0, 0);
      ctx.lineTo(100, 0);
      ctx.stroke();
      ctx.restore();

      ctx.save();
      ctx.rotate((Math.PI / 6) * hour + (Math.PI / 360) * minute);
      ctx.lineWidth = 4;
      ctx.beginPath();
      ctx.moveTo(0, 0);
      ctx.lineTo(80, 0);
      ctx.stroke();
      ctx.restore();
      requestAnimationFrame(clock_draw);
   }
   clock_draw();
}